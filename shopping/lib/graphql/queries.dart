const String getAllItems = r'''
query {
   allItems{
    id
    name
    imageUrl
    price
    Voucher{
      free_shipping
      discount
    }
    Specification{
      id
      dimensions
      weight
      build
    	display_type
      size
      resolution
      protection
      OS
      chipset
      CPU
      GPU
      internal
      main_camera
      selfie_camera
      loudspeaker
      earphone_jack
      features
      colors
      battery_type
      charging
    }
  }
}
''';

const String getAllStore = r'''
query {
    allStores{
    id
    name
    imageUrl
    Items{
      id
      name
      imageUrl
      price
      Voucher{
        discount
      }
      Specification{
      id
      dimensions
      weight
      build
    	display_type
      size
      resolution
      protection
      OS
      chipset
      CPU
      GPU
      internal
      main_camera
      selfie_camera
      loudspeaker
      earphone_jack
      features
      colors
      battery_type
      charging
    }
    }
  }
}
''';

const String getOneStore = r'''
query getOneStore($id:ID!){
  Store(id: $id){
    id
    name
    imageUrl
    Items{
      id
      store_id
      name
      imageUrl
      price
      Voucher{
        discount
      }
    }
  }
}
''';

const String getOneItem = r'''
query getOneItem($id:ID!){
  Item(id: $id){
    id
    name
    imageUrl
    price
    stocks
    Specification{
      id
      dimensions
      weight
      build
    	display_type
      size
      resolution
      protection
      OS
      chipset
      CPU
      GPU
      internal
      main_camera
      selfie_camera
      loudspeaker
      earphone_jack
      features
      colors
      battery_type
      charging
    }
    Voucher {
      id
      free_shipping
      discount
    }
    Reviews{
      id
      comment
    }
  }

  allStores{
    id
    name 
    Items{
      id
    }
  }
}
''';

const String getNumberOfItems = r'''
query {
  allItems{
    id
  }
}
''';
