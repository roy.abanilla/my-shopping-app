const String createItem = r'''
mutation createItemMutation($id: ID!, $store_id: ID!, $name: String!, $imageUrl: String!, 
  $price: Int!, $stocks: Int!, $specification_id: ID!, $voucher_id: ID!, $dimensions: String!, 
  $weight: String!, $build: String!, $display_type: String!, $size: String!, $resolution: String!, 
  $protection: String!, $os: String!, $chipset: String!, $cpu: String!, $gpu: String!, $internal: String!,
  $main_camera: String!, $selfie_camera: String!, $loudspeaker: String!, $earphone_jack: String!,
  $features: String!, $colors: String!, $battery_type: String!, $charging: String!, $discount: Int!,
  $free_shipping: String!) {
  
  createItem(id: $id, store_id: $store_id, name: $name, imageUrl: $imageUrl, price: $price, stocks: $stocks,
    specification_id: $specification_id, voucher_id: $voucher_id) {
    id
    store_id
  }
    
  createSpecification(id: $specification_id, dimensions: $dimensions, weight: $weight, build: $build,
    display_type: $display_type, size: $size, resolution: $resolution, protection: $protection, OS: $os,
    chipset: $chipset, CPU: $cpu, GPU: $gpu, internal: $internal, main_camera: $main_camera, selfie_camera: $selfie_camera, 
    loudspeaker: $loudspeaker, earphone_jack: $earphone_jack, features: $features, colors: $colors, battery_type: $battery_type, 
    charging: $charging){ 
    id
  }
    
  createVoucher(id: $voucher_id, discount: $discount, free_shipping: $free_shipping) {
    id
  }
    
}
''';

const String updateItem = r'''
mutation updateItem($id: ID!, $store_id: ID!, $name: String!, $imageUrl: String!, 
  $price: Int!, $stocks: Int!, $specification_id: ID!, $voucher_id: ID!, $dimensions: String!, 
  $weight: String!, $build: String!, $display_type: String!, $size: String!, $resolution: String!, 
  $protection: String!, $os: String!, $chipset: String!, $cpu: String!, $gpu: String!, $internal: String!,
  $main_camera: String!, $selfie_camera: String!, $loudspeaker: String!, $earphone_jack: String!,
  $features: String!, $colors: String!, $battery_type: String!, $charging: String!, $discount: Int!,
  $free_shipping: String!) {
  
  updateItem(id: $id, store_id: $store_id, name: $name, imageUrl: $imageUrl, price: $price, stocks: $stocks,
    specification_id: $specification_id, voucher_id: $voucher_id) {
    id
    store_id
  }
    
  updateSpecification(id: $specification_id, dimensions: $dimensions, weight: $weight, build: $build,
    display_type: $display_type, size: $size, resolution: $resolution, protection: $protection, OS: $os,
    chipset: $chipset, CPU: $cpu, GPU: $gpu, internal: $internal, main_camera: $main_camera, selfie_camera: $selfie_camera, 
    loudspeaker: $loudspeaker, earphone_jack: $earphone_jack, features: $features, colors: $colors, battery_type: $battery_type, 
    charging: $charging){ 
    id
  }
    
  updateVoucher(id: $voucher_id, discount: $discount, free_shipping: $free_shipping) {
    id
  }
    
}

''';
