import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/provider/add_and_edit_item_provider.dart';
import 'package:shopping/provider/checkbox_provider.dart';
import 'package:shopping/provider/one_item_provider.dart';
import 'package:shopping/provider/one_store_provider.dart';
import 'package:shopping/screens/home_screen.dart';
import 'package:shopping/widgets/custom_drop_down.dart';
import 'package:shopping/widgets/custom_text_formField.dart';

import 'checkbox.dart';

class AddAndEditScreen extends StatefulWidget {
  final String formType;

  const AddAndEditScreen({Key key, this.formType = 'EDIT'}) : super(key: key);

  @override
  _AddAndEditScreenState createState() => _AddAndEditScreenState();
}

class _AddAndEditScreenState extends State<AddAndEditScreen> {
  final _formKey = GlobalKey<FormState>();
  String choosenOS;
  List<String> newInternalArray = [];

  String choosenLoudspeaker;
  String choosenEarphoneJack;
  String freeShip;

  final List<String> osList = [
    'Android 8 (Oreo)',
    'Android 9 (Pie)',
    'Android 10',
    'Android 11',
  ];

  final List<String> yesOrNo = ['Yes', 'No'];

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final addAndEditItemProvider = watch(addAndEditItemCNP);
        final storeProvider = watch(oneStoreCNP);
        final oneItemProvider = watch(oneItemCNP);
        final checkboxProvider = watch(checkboxCNP);

        return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.white),
            centerTitle: false,
            title: Text(
              widget.formType == 'EDIT' ? 'Edit Item' : 'Add Item',
              style: TextStyle(color: Colors.white),
            ),
            leading: TextButton(
                onPressed: () {
                  showExitAlert(checkboxProvider);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                )),
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTop(addAndEditItemProvider),
                  Center(
                    child: SizedBox(
                      width: 300,
                      child: Divider(
                        color: Colors.black,
                        thickness: 0.5,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _buildSpecs(addAndEditItemProvider, checkboxProvider),
                  Center(
                    child: SizedBox(
                      width: 300,
                      child: Divider(
                        color: Colors.black,
                        thickness: 0.5,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _buildVoucher(addAndEditItemProvider),
                  SizedBox(
                    height: 20,
                  ),
                  _buildOnSavedButton(addAndEditItemProvider, storeProvider,
                      oneItemProvider, checkboxProvider),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildTop(AddAndEditItemProvider addAndEditItemProvider) {
    return Form(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          Row(
            children: [
              Text('Store Name:'),
              SizedBox(
                width: 20,
              ),
              Text(
                addAndEditItemProvider.storeName,
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          CustomTextFormField(
            label: 'Item Name:',
            hintText: 'Poco X3',
            controller: addAndEditItemProvider.itemNameController,
          ),
          CustomTextFormField(
            label: 'Image URL:',
            hintText: 'https:Image/image/',
            controller: addAndEditItemProvider.imageUrlController,
          ),
          CustomTextFormField(
            label: 'Price (in ₱):',
            hintText: '₱ 6490',
            textInputType: TextInputType.number,
            controller: addAndEditItemProvider.priceController,
          ),
          CustomTextFormField(
            label: 'Stocks:',
            hintText: '123',
            textInputType: TextInputType.number,
            controller: addAndEditItemProvider.stocksController,
          ),
        ],
      ),
    );
  }

  Widget _buildSpecs(AddAndEditItemProvider addAndEditItemProvider,
      CheckboxProvider checkboxProvider) {
    return Column(
      children: [
        Text(
          'Specification',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20,
        ),
        CustomTextFormField(
            label: 'Dimensions',
            hintText: '164.4 x 75.6 x 9.3 mm',
            controller: addAndEditItemProvider.dimensionsController),
        CustomTextFormField(
            label: 'Weight',
            hintText: '198 g (6.98 oz)',
            controller: addAndEditItemProvider.weightController),
        CustomTextFormField(
            label: 'Build',
            hintText:
                'Glass front (Gorilla Glass 3+), plastic back, plastic frame',
            controller: addAndEditItemProvider.buildController),
        CustomTextFormField(
            label: 'Display Type',
            hintText: 'IPS LCD',
            controller: addAndEditItemProvider.displayTypeController),
        CustomTextFormField(
            label: 'Size',
            hintText: '6.5 inches, 102.8 cm2 (~82.7% screen-to-body ratio)',
            controller: addAndEditItemProvider.sizeController),
        CustomTextFormField(
            label: 'Resolution',
            hintText: '720 x 1600 pixels,',
            controller: addAndEditItemProvider.resolutionController),
        CustomTextFormField(
            label: 'Protection',
            hintText: 'Corning Gorilla Glass 3+',
            controller: addAndEditItemProvider.protectionController),
        Column(
          children: [
            Row(
              children: [
                Text('OS'),
                Spacer(),
                CustomDropDown(
                    variable:
                        addAndEditItemProvider.osController.text.isNotEmpty
                            ? addAndEditItemProvider.osController.text
                            : null,
                    function: (val) {
                      setState(() {
                        addAndEditItemProvider.osController.text = val;
                      });
                    },
                    list: osList)
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
        CustomTextFormField(
            label: 'Chipset',
            hintText: 'Qualcomm SDM665 Snapdragon 665 (11 nm)',
            controller: addAndEditItemProvider.chipsetController),
        CustomTextFormField(
            label: 'CPU',
            hintText:
                'Octa-core (4x2.0 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver)',
            controller: addAndEditItemProvider.cpuController),
        CustomTextFormField(
            label: 'GPU',
            hintText: 'Adreno 610',
            controller: addAndEditItemProvider.gpuController),
        Column(
          children: [
            Row(
              children: [
                Text('Internal'),
                Column(
                  children: [
                    TextButton(
                      style: TextButton.styleFrom(
                          padding: EdgeInsets.all(0),
                          minimumSize: Size(30, 30)),
                      onPressed: () {
                        print('tap edit internal');

                        // MUST CHANGE THE ARRAY
                        for (var i = 0;
                            i < checkboxProvider.choosenList.length;
                            i++) {
                          for (var j = 0;
                              j < checkboxProvider.list.length;
                              j++) {
                            if (checkboxProvider.list[j].title ==
                                checkboxProvider.choosenList[i]) {
                              setState(() {
                                checkboxProvider.list[j].value = true;
                              });
                            }
                          }
                        }
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CheckboxScreen()));
                      },
                      child: Icon(
                        Icons.edit,
                        color: Colors.black,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                ),
                Spacer(),
                Container(
                  padding: EdgeInsets.all(0),
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: checkboxProvider.choosenList.isEmpty
                      ? Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border:
                                  Border.all(width: 0.5, color: Colors.grey)),
                          child: Text(
                            'Empty',
                            style: TextStyle(fontSize: 12),
                          ),
                        )
                      : SizedBox(
                          height: checkboxProvider.choosenList.length *
                              40.toDouble(),
                          child: ListView.separated(
                            physics: NeverScrollableScrollPhysics(),
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(
                                height: 10,
                              );
                            },
                            itemCount: checkboxProvider.choosenList.length,
                            itemBuilder: (context, int index) {
                              return Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                        width: 0.5, color: Colors.grey)),
                                child: Text(
                                  checkboxProvider.choosenList[index]
                                      .toString(),
                                  style: TextStyle(fontSize: 12),
                                ),
                              );
                            },
                          ),
                        ),
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
        CustomTextFormField(
            label: 'Main Camera',
            hintText: '12 MP, f/1.8, (wide), 1/2.9in, 1.25µm, PDAF',
            controller: addAndEditItemProvider.mainCameraController),
        CustomTextFormField(
            label: 'Selfie Camera',
            hintText: '13 MP, f/2.0, 26mm (wide), 1/3.1in, 1.12µm',
            controller: addAndEditItemProvider.selfieCameraController),
        Column(
          children: [
            Row(
              children: [
                Text('Loudspeaker'),
                Spacer(),
                CustomDropDown(
                    variable: addAndEditItemProvider
                            .loudspeakerController.text.isNotEmpty
                        ? addAndEditItemProvider.loudspeakerController.text
                        : null,
                    function: (val) {
                      setState(() {
                        addAndEditItemProvider.loudspeakerController.text = val;
                      });
                    },
                    list: yesOrNo)
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
        Column(
          children: [
            Row(
              children: [
                Text('Earphone Jack'),
                Spacer(),
                CustomDropDown(
                    variable: addAndEditItemProvider
                            .earphoneJackController.text.isNotEmpty
                        ? addAndEditItemProvider.earphoneJackController.text
                        : null,
                    function: (val) {
                      setState(() {
                        addAndEditItemProvider.earphoneJackController.text =
                            val;
                      });
                    },
                    list: yesOrNo)
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
        CustomTextFormField(
            label: 'Features',
            hintText:
                'Fingerprint (rear-mounted), accelerometer, gyro, proximity, compass',
            controller: addAndEditItemProvider.featuresController),
        CustomTextFormField(
            label: 'Colors',
            hintText: 'Crystal Purple, Crystal Blue',
            controller: addAndEditItemProvider.colorsController),
        CustomTextFormField(
            label: 'Battery Type',
            hintText: 'Li-Po 5000 mAh, non-removable',
            controller: addAndEditItemProvider.batteryTypeController),
        CustomTextFormField(
            label: 'Charging',
            hintText: 'Charging 10W',
            controller: addAndEditItemProvider.chargingController),
      ],
    );
  }

  Widget _buildVoucher(AddAndEditItemProvider addAndEditItemProvider) {
    return Column(
      children: [
        Text(
          'Voucher',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20,
        ),
        Column(
          children: [
            Row(
              children: [
                Text('Free Shipping'),
                Spacer(),
                CustomDropDown(
                    variable: addAndEditItemProvider
                            .freeShippingController.text.isNotEmpty
                        ? addAndEditItemProvider.freeShippingController.text
                        : null,
                    function: (val) {
                      setState(() {
                        addAndEditItemProvider.freeShippingController.text =
                            val;
                      });
                    },
                    list: yesOrNo)
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
        CustomTextFormField(
            label: 'Discount',
            hintText: '20',
            textInputType: TextInputType.number,
            controller: addAndEditItemProvider.discountController),
      ],
    );
  }

  Widget _buildOnSavedButton(
      AddAndEditItemProvider addAndEditItemProvider,
      GetOneStoreProvider storeProvider,
      GetOneItemProvider oneItemProvider,
      CheckboxProvider checkboxProvider) {
    return Row(
      children: [
        Spacer(),
        ElevatedButton(
            onPressed: () async {
              print('tap Save button');
              await addAndEditItemProvider.getTotalItemsNumber();
              print(
                  'on save here, store id: ${addAndEditItemProvider.storeId}');
              if (widget.formType == 'EDIT') {
                addAndEditItemProvider.setIds(oneItemProvider.id.toString());
                print(
                    'on save here, Item id: ${addAndEditItemProvider.itemId}');
              } else {
                addAndEditItemProvider.setIds(
                    (addAndEditItemProvider.numberOfItems + 1).toString());
                print(
                    'on save here, Item id: ${addAndEditItemProvider.itemId}');
              }
              //validate
              if (_formKey.currentState.validate()) {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Confirmation',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 18),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                Icons.notification_important,
                                color: Colors.blue,
                              )
                            ],
                          ),
                          content: widget.formType == 'EDIT'
                              ? Text(
                                  'Are you sure you want to Edit ${addAndEditItemProvider.itemNameController.text}?\nNOTE: Make sure all the fields are correct.',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12),
                                )
                              : Text(
                                  'Are you sure you want to Add ${addAndEditItemProvider.itemNameController.text}?\nNOTE: Make sure all the fields are correct.',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12),
                                ),
                          actions: [
                            ElevatedButton(
                                onPressed: () async {
                                  print('tapped ok');
                                  if (widget.formType == 'EDIT') {
                                    print('sucess editing ITem');
                                    await addAndEditItemProvider
                                        .updateOneItem();
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                HomeScreen()));
                                  } else {
                                    print('sucess adding item');
                                    await addAndEditItemProvider
                                        .createOneItem();
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                HomeScreen()));
                                  }
                                  // clearing the checkbox
                                  for (var i = 0;
                                      i < checkboxProvider.list.length;
                                      i++) {
                                    checkboxProvider.list[i].value = false;
                                  }
                                  // changing the internalController.text
                                  addAndEditItemProvider
                                          .internalController.text =
                                      checkboxProvider.choosenList
                                          .join(',')
                                          .toString();
                                },
                                child: Text(
                                  'Ok',
                                  style: TextStyle(color: Colors.white),
                                )),
                            TextButton(
                                onPressed: () {
                                  print('tap cancel');
                                  Navigator.pop(context);
                                },
                                child: Text('Cancel'))
                          ],
                        ));
              }
            },
            child: Text(
              'Save',
              style: TextStyle(color: Colors.white),
            )),
        SizedBox(
          width: 10,
        ),
        TextButton(
            onPressed: () {
              print('tap cancel button');
              showExitAlert(checkboxProvider);
            },
            child: Text('Cancel'))
      ],
    );
  }

  void showExitAlert(CheckboxProvider checkboxProvider) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Warning',
            ),
            SizedBox(
              width: 10,
            ),
            Icon(
              Icons.warning_amber_outlined,
              color: Colors.red,
            )
          ],
        ),
        content: Text(
          'Are you sure you want to Exit right now?\nYour changes will not be made!',
          style: TextStyle(fontSize: 12),
          textAlign: TextAlign.center,
        ),
        actions: [
          ElevatedButton(
              onPressed: () {
                // clear the checkbox
                for (var i = 0; i < checkboxProvider.list.length; i++) {
                  checkboxProvider.list[i].value = false;
                }
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: Text(
                'Yes',
                style: TextStyle(color: Colors.white),
              )),
          TextButton(
              onPressed: () {
                print('tap cancel');
                Navigator.pop(context);
              },
              child: Text('No'))
        ],
      ),
    );
  }
}
