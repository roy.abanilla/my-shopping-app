import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/item_model.dart';
import 'package:shopping/provider/add_to_cart_provider.dart';
import 'package:shopping/provider/list_items_provider.dart';

class AddToCartScreeen extends StatefulWidget {
  @override
  _AddToCartScreeenState createState() => _AddToCartScreeenState();
}

class _AddToCartScreeenState extends State<AddToCartScreeen> {
  final GetListOfItemProvider listOfItemProvider = GetListOfItemProvider();
  List<Item> items = [];
  List<Item> cartItems = [];

  void getData() async {
    await listOfItemProvider.getListOfItem();
    setState(() {
      items = listOfItemProvider.listOfItems;
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final addToCartProvider = watch(addToCartCNP);
        final idList = addToCartProvider.addToCartList.toSet().toList();
        for (var i = 0; i < idList.length; i++) {
          for (var j = 0; j < items.length; j++) {
            if (idList[i] == items[j].id) {
              cartItems.add(items[j]);
              cartItems = cartItems.toSet().toList();
              print('here filtering');
            }
          }
        }
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.blue),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              print('tap checkout');
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(
                        'Sorry 😞',
                        textAlign: TextAlign.center,
                      ),
                      content: Text(
                        'Payment feature is currently on progress.',
                        textAlign: TextAlign.center,
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('Ok'))
                      ],
                    );
                  });
            },
            child: Icon(
              Icons.payment,
              color: Colors.white,
            ),
          ),
          body: Container(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Shopping Cart',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  cartItems.isEmpty
                      ? Center(
                          heightFactor: 1.5,
                          child: Image(
                            height: 300,
                            width: 300,
                            image: AssetImage('lib/assets/empty_cart.png'),
                          ))
                      : ListView.separated(
                          physics: ClampingScrollPhysics(),
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: 20,
                            );
                          },
                          shrinkWrap: true,
                          itemCount: cartItems.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _buildCart(
                                cartItems[index], addToCartProvider);
                          }),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  _buildCart(Item item, AddToCartProvider addToCartProvider) {
    return Material(
      borderRadius: BorderRadius.circular(10),
      elevation: 4.0,
      child: Container(
        height: 120,
        padding: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: Color(0xFFe1effa),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            Container(
              width: 100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.white),
              child: Center(
                child: Container(
                  height: 90,
                  width: 60,
                  child: Image(
                    image: NetworkImage(item.imageUrl),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 15),
                  Text(
                    item.name,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                  Spacer(),
                  Row(
                    children: [
                      Container(
                        height: 30,
                        width: 30,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(0)),
                            onPressed: () {
                              if (addToCartProvider.cartCount(item.id) == 1) {
                                _showRemoveCart(item, addToCartProvider);
                              } else {
                                addToCartProvider.removeToCart(item.id);
                                print('minus 1');
                              }
                              print(addToCartProvider.addToCartList);
                              //
                            },
                            child: Icon(
                              Icons.remove,
                              size: 15,
                              color: Colors.white,
                            )),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(addToCartProvider.cartCount(item.id).toString()),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        height: 30,
                        width: 30,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(0)),
                            onPressed: () {
                              print('plus 1');
                              addToCartProvider.addToCart(item.id);
                            },
                            child: Icon(
                              Icons.add,
                              size: 15,
                              color: Colors.white,
                            )),
                      ),
                      Spacer(),
                      Text(
                        '₱ ${item.price.toString()}',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showRemoveCart(Item item, AddToCartProvider addToCartProvider) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: Text(
                'Do you want to remove this product?',
                style: TextStyle(fontSize: 13),
                textAlign: TextAlign.center,
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      print('tap Yes');
                      addToCartProvider.removeToCart(item.id);
                      print('minus 1');
                      setState(() {
                        cartItems
                            .removeWhere((element) => element.id == item.id);
                      });
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Yes',
                      style: TextStyle(color: Colors.red),
                    )),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('No'))
              ],
            );
          });
        });
  }
}
