import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/provider/user_provider.dart';
import 'package:shopping/screens/home_screen.dart';

import 'sign_up.dart';

// ignore: must_be_immutable
class SignInScreen extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final userProvider = watch(userCNP);
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.2,
                  ),
                  Text(
                    'Shopping App',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                  TextFormField(
                    controller: _emailController,
                    style: TextStyle(fontSize: 15),
                    decoration: InputDecoration(
                      hintText: 'Email or Phone Number / e.g. user@gmail.com',
                      hintStyle: TextStyle(fontSize: 15),
                    ),
                    validator: (e) {
                      if (e.isEmpty) {
                        return '*Email or Phone Number is required.';
                      }
                      return null;
                    },
                    // validator: (value) {
                    //   if (value.isEmpty) {
                    //     return '*Email is required.';
                    //   } else {
                    //     final pattern =
                    //         r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
                    //     final regExp = RegExp(pattern);

                    //     if (!regExp.hasMatch(value)) {
                    //       return 'Enter a valid mail';
                    //     } else {
                    //       return null;
                    //     }
                    //   }
                    // },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    style: TextStyle(fontSize: 15),
                    decoration: InputDecoration(
                      hintText: 'Password / e.g. 123qwe',
                      hintStyle: TextStyle(fontSize: 15),
                    ),
                    validator: (e) {
                      if (e.isEmpty) {
                        return '*Password is required.';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        signIn(context, userProvider);
                      },
                      child: SizedBox(
                        width: 50,
                        child: Text(
                          'Login',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  SizedBox(
                    height: 20,
                  ),
                  Text('No account yet?'),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                      onTap: () {
                        print('tap create an account');
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SignUpScreen()));
                      },
                      child: Text(
                        'Create an account using phone number',
                        style: TextStyle(color: Colors.blue),
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future signIn(BuildContext context, UserProvider userProvider) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        await _firebaseAuth
            .signInWithEmailAndPassword(
                email: _emailController.text,
                password: _passwordController.text)
            .then((value) {
          userProvider.setId(value.user.tenantId);
          userProvider.setEmail(value.user.email);
          userProvider.setImageUrl(value.user.photoURL);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomeScreen()));
        });
      } catch (e) {
        print('error here: ${e.toString()}');
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Warning'),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.warning,
                      color: Colors.red,
                    )
                  ],
                ),
                content: Text(
                  e.toString(),
                  textAlign: TextAlign.center,
                ),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Ok'))
                ],
              );
            });
      }
    }
  }
}
