import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/provider/user_provider.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final userProvider = watch(userCNP);
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: Container(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  height: 150,
                  color: Color(0xff3fa4f2),
                  child: Column(
                    children: [
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: userProvider.imageUrl == null ?? true
                            ? AssetImage('lib/assets/default-image.jpeg')
                            : NetworkImage(userProvider.imageUrl),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        userProvider.email,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
