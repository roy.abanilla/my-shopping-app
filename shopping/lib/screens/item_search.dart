import 'package:flutter/material.dart';
import 'package:shopping/models/item_model.dart';
import 'package:shopping/widgets/item_card_widget.dart';

class ItemSearch extends SearchDelegate<String> {
  var list;
  List<Item> _itemList;

  ItemSearch(
    this._itemList,
  );

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.black,
        ),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        color: Colors.black,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final List<Item> suggestionList = query.isEmpty
        ? _itemList
        : _itemList
            .where(
                (item) => item.name.toLowerCase().contains(query.toLowerCase()))
            .toList();
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: GridView(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisSpacing: 10, mainAxisSpacing: 10, crossAxisCount: 2),
        children: suggestionList.map((item) {
          return ItemCard(item: item);
        }).toList(),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> _itemTitles = _itemList.map((e) => e.name).toList();
    List<String> suggestionList = query.isEmpty
        ? _itemTitles
        : _itemTitles
            .where((item) => item.toLowerCase().contains(query.toLowerCase()))
            .toList();
    suggestionList.sort((a, b) => a.compareTo(b));
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: suggestionList.length,
        itemBuilder: (context, int index) => ListTile(
              onTap: () {
                query = suggestionList[index];
              },
              leading: Icon(Icons.phone_android),
              title: Text(suggestionList[index]),
            ));
  }
}
