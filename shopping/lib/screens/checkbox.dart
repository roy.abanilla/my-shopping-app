import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/provider/add_and_edit_item_provider.dart';
import 'package:shopping/provider/checkbox_provider.dart';

class CheckboxScreen extends StatefulWidget {
  @override
  _CheckboxScreenState createState() => _CheckboxScreenState();
}

class _CheckboxScreenState extends State<CheckboxScreen> {
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final checkboxProvider = watch(checkboxCNP);
        final addAndEditItemProvider = watch(addAndEditItemCNP);
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.blue),
            leading: IconButton(
              onPressed: () {
                print('tap back');
                _saveAndBack(checkboxProvider, addAndEditItemProvider);
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
          ),
          body: Container(
            padding: EdgeInsets.all(20),
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      child: Text(
                    'Internal',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )),
                  Column(
                    children: checkboxProvider.list
                        .map((item) => CheckboxListTile(
                            activeColor: Colors.blue,
                            title: Text(item.title),
                            value: item.value,
                            onChanged: (value) {
                              setState(() {
                                item.value = !item.value;
                              });
                            }))
                        .toList(),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        ElevatedButton(
                            onPressed: () {
                              print('tap save');
                              _saveAndBack(
                                  checkboxProvider, addAndEditItemProvider);
                            },
                            child: Text('Save',
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white))),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _saveAndBack(CheckboxProvider checkboxProvider,
      AddAndEditItemProvider addAndEditItemProvider) {
    for (var j = 0; j < checkboxProvider.list.length; j++) {
      print(
          '${checkboxProvider.list[j].title}: ${checkboxProvider.list[j].value}');

      if (checkboxProvider.list[j].value == true) {
        checkboxProvider.addToList(checkboxProvider.list[j].title);
      } else {
        checkboxProvider.removeToList(checkboxProvider.list[j].title);
      }
    }
    checkboxProvider
        .setChoosenList(checkboxProvider.choosenList.toSet().toList());
    print(checkboxProvider.choosenList);
    addAndEditItemProvider.internalController.text =
        checkboxProvider.choosenList.join(',').toString();
    Navigator.pop(context);
  }
}
