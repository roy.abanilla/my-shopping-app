import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/item_model.dart';
import 'package:shopping/models/store_model.dart';
import 'package:shopping/provider/list_stores_provider.dart';
import 'package:shopping/provider/list_items_provider.dart';
import 'package:shopping/provider/one_item_provider.dart';
import 'package:shopping/provider/one_store_provider.dart';
import 'package:shopping/screens/add_to_cart_screen.dart';
import 'package:shopping/screens/item_screen.dart';
import 'package:shopping/screens/settings_screen.dart';
import 'package:shopping/screens/sign_in_screen.dart';
import 'package:shopping/screens/store_screen.dart';
import 'package:shopping/widgets/discount_ribbon.dart';
import 'package:shopping/widgets/item_card_widget.dart';

import 'item_search.dart';
import 'list_store_screen.dart';
// import 'package:shopping/provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GetListOfStoreProvider storeProvider = GetListOfStoreProvider();
  final GetListOfItemProvider listOfItemProvider = GetListOfItemProvider();
  List<OneStore> listOfStore = [];
  List<Item> discountedItems = [];
  List<Item> listOfItems = [];
  final auth = FirebaseAuth.instance;

  bool loading = false;
  final Color mainColor = Color(0xff3fa4f2);

  void getData() async {
    setState(() {
      loading = true;
    });
    await storeProvider.getListOfStore();
    await listOfItemProvider.getListOfItem();
    setState(() {
      listOfStore = storeProvider.listOfStore;
      discountedItems = listOfItemProvider.discountedItems;
      listOfItems = listOfItemProvider.listOfItems;
    });

    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final oneStoreProvider = watch(oneStoreCNP);
        final oneItemProvider = watch(oneItemCNP);

        return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.white),
            elevation: 0,
            leading: PopupMenuButton(
              icon: Icon(Icons.logout),
              onSelected: (value) {
                if (value == 'logout') {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text(
                            'Are you sure you want to logout now?',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 14),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () async {
                                  await auth.signOut().then((value) =>
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignInScreen())));
                                },
                                child: Text(
                                  'Yes',
                                  style: TextStyle(color: Colors.red),
                                )),
                            TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('No'))
                          ],
                        );
                      });
                }
              },
              itemBuilder: (context) {
                return [
                  PopupMenuItem(
                    child: Text('Logout'),
                    value: 'logout',
                  )
                ];
              },
            ),
            actions: [
              IconButton(
                  icon: Icon(Icons.search),
                  color: Colors.white,
                  onPressed: () {
                    print('searching...');
                    showSearch(
                        context: context, delegate: ItemSearch(listOfItems));
                  }),
              IconButton(
                  icon: Icon(Icons.shopping_cart),
                  color: Colors.white,
                  onPressed: () {
                    print('tap cart button');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddToCartScreeen()));
                  }),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingScreen()));
            },
            child: Icon(
              Icons.settings,
              color: Colors.white,
            ),
          ),
          body: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 1400,
              padding: EdgeInsets.only(top: 10),
              color: mainColor,
              child: Column(
                children: [
                  _buildTopWidget(oneStoreProvider),
                  _buildBottomWidget(oneItemProvider)
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildTopWidget(oneStoreProvider) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Row(
            children: [
              Text(
                'Shop by store',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              Spacer(),
              TextButton(
                  onPressed: () {
                    print('tap See All');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ListOfStoreWidget(listOfStore: listOfStore)));
                  },
                  child: Text(
                    'See all ',
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ))
            ],
          ),
        ),
        listOfStore.isEmpty
            ? Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              )
            : Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: SizedBox(
                  height: 50,
                  child: ListView.separated(
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          width: 10,
                        );
                      },
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: listOfStore.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _logoCard(listOfStore[index], oneStoreProvider);
                      }),
                ),
              ),
        SizedBox(height: 30),
      ],
    );
  }

  Widget _logoCard(OneStore store, oneStoreProvider) {
    return InkWell(
      onTap: () async {
        print('tap ${store.name}');
        oneStoreProvider.setStoreId(store.id);
        await oneStoreProvider.getStore();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => OneStoreScreen()));
      },
      child: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: NetworkImage(store.imageUrl), fit: BoxFit.cover)),
      ),
    );
  }

  Widget _buildBottomWidget(GetOneItemProvider oneItemProvider) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  'BEST OFFER',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
                Spacer(),
              ],
            ),
            SizedBox(height: 10),
            discountedItems.isEmpty
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SizedBox(
                    height: 200,
                    child: ListView.separated(
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(
                            width: 20,
                          );
                        },
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: discountedItems.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _buildSaleCard(
                              discountedItems[index], oneItemProvider);
                        }),
                  ),
            SizedBox(height: 30),
            Text(
              'DAILY DISCOVER',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            listOfItems.isEmpty
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Expanded(
                    child: GridView(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          crossAxisCount: 2),
                      children: listOfItems.map((item) {
                        return ItemCard(item: item);
                      }).toList(),
                    ),
                  )
          ],
        ),
      ),
    );
  }

  Widget _buildSaleCard(Item item, GetOneItemProvider oneItemProvider) {
    return InkWell(
        onTap: () async {
          print('tap ${item.name}');
          oneItemProvider.setItemId(item.id);
          await oneItemProvider.getItem();
          print('item id : ${item.id.toString()}');
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => ItemScreen()));
        },
        child: Container(
          padding: EdgeInsets.all(10),
          height: 200,
          width: 250,
          decoration: BoxDecoration(
            color: Color(0xFFe1effa),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 100,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image(
                            fit: BoxFit.fill,
                            width: 120,
                            height: 130,
                            image: NetworkImage(item.imageUrl))),
                    Text(
                      item.name,
                      maxLines: 3,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: item.name.length > 15 ? 12 : 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  DiscountRibbonWidget(
                    discount: item.discount,
                    diameter: 70,
                    fontsize: 15,
                  ),
                  Text(
                    '₱ ${item.price.toString()}',
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  if (item.freeShipping.isNotEmpty)
                    Column(
                      children: [
                        SizedBox(
                          height: 5,
                        ),
                        Icon(
                          Icons.local_shipping,
                          color: Colors.green,
                          size: 30,
                        ),
                      ],
                    ),
                ],
              ),
            ],
          ),
        ));
  }
}
