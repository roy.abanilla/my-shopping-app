import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/store_model.dart';
import 'package:shopping/provider/one_store_provider.dart';
import 'package:shopping/screens/store_screen.dart';

class ListOfStoreWidget extends StatefulWidget {
  final List<OneStore> listOfStore;
  ListOfStoreWidget({@required this.listOfStore});
  @override
  _ListOfStoreWidgetState createState() => _ListOfStoreWidgetState();
}

class _ListOfStoreWidgetState extends State<ListOfStoreWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      final oneStoreProvider = watch(oneStoreCNP);
      return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            'List Of Store',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: widget.listOfStore.isEmpty
              ? Center(child: CircularProgressIndicator())
              : GridView(
                  physics: ClampingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      crossAxisCount: 3),
                  children: widget.listOfStore
                      .map((store) => _buildLogo(store, oneStoreProvider))
                      .toList(),
                ),
        ),
      );
    });
  }

  Widget _buildLogo(OneStore store, oneStoreProvider) {
    return InkWell(
      onTap: () async {
        oneStoreProvider.setStoreId(store.id);
        await oneStoreProvider.getStore();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => OneStoreScreen()));
      },
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Color(0xFFe1effa),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: NetworkImage(store.imageUrl),
                      fit: BoxFit.fitHeight)),
            ),
            Spacer(),
            Text(
              store.name,
              maxLines: 2,
              style: TextStyle(fontSize: 12),
            ),
          ],
        ),
      ),
    );
  }
}
