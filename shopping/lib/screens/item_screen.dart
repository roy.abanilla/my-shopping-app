import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/review_model.dart';
import 'package:shopping/models/specification_model.dart';
import 'package:shopping/models/voucher_model.dart';
import 'package:shopping/provider/add_and_edit_item_provider.dart';
import 'package:shopping/provider/add_to_cart_provider.dart';
import 'package:shopping/provider/checkbox_provider.dart';
import 'package:shopping/provider/one_item_provider.dart';
import 'package:shopping/screens/add_and_edit_item_screen.dart';
import 'package:shopping/widgets/label.dart';

class ItemScreen extends StatefulWidget {
  @override
  _ItemScreenState createState() => _ItemScreenState();
}

class _ItemScreenState extends State<ItemScreen> {
  Specification specification;
  Voucher voucher;
  List<Review> reviews;

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final oneItemProvider = watch(oneItemCNP);
        final addAndEditItemProvider = watch(addAndEditItemCNP);
        final addToCartProvider = watch(addToCartCNP);
        final checkboxProvider = watch(checkboxCNP);
        specification = oneItemProvider.specification;
        voucher = oneItemProvider.voucher;
        reviews = oneItemProvider.reviews;

        return Scaffold(
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                print('tapped add to Cart');
                addToCartProvider.addToCart(oneItemProvider.id);
                print(addToCartProvider.addToCartList);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    duration: Duration(seconds: 1),
                    content: Text('Added to cart sucessfully.')));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.shopping_basket,
                    color: Colors.white,
                  ),
                  Text(
                    'Add to cart',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 8, color: Colors.white),
                  )
                ],
              )),
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.white),
            centerTitle: false,
            title: Text(
              'Item',
              style: TextStyle(color: Colors.white),
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    addAndEditItemProvider
                        .setStoreName(oneItemProvider.storeName);
                    addAndEditItemProvider.setStoreId(oneItemProvider.storeId);
                    addAndEditItemProvider.itemNameController.text =
                        oneItemProvider.name;
                    addAndEditItemProvider.imageUrlController.text =
                        oneItemProvider.imageUrl;
                    addAndEditItemProvider.priceController.text =
                        oneItemProvider.price.toString();
                    addAndEditItemProvider.stocksController.text =
                        oneItemProvider.stocks.toString();
                    //
                    final specs = oneItemProvider.specification;
                    addAndEditItemProvider.dimensionsController.text =
                        specs.dimensions;
                    addAndEditItemProvider.weightController.text = specs.weight;
                    addAndEditItemProvider.buildController.text = specs.build;
                    addAndEditItemProvider.displayTypeController.text =
                        specs.displayType;
                    addAndEditItemProvider.sizeController.text = specs.size;
                    addAndEditItemProvider.resolutionController.text =
                        specs.resolution;
                    addAndEditItemProvider.protectionController.text =
                        specs.protection;
                    addAndEditItemProvider.osController.text =
                        specs.operatingSystem;
                    addAndEditItemProvider.chipsetController.text =
                        specs.chipset;
                    addAndEditItemProvider.cpuController.text = specs.cpu;
                    addAndEditItemProvider.gpuController.text = specs.gpu;
                    //
                    // addAndEditItemProvider.internalController.text =
                    //     specs.internal;
                    checkboxProvider.setChoosenList(specs.internal.split(','));
                    addAndEditItemProvider.mainCameraController.text =
                        specs.mainCamera;
                    addAndEditItemProvider.selfieCameraController.text =
                        specs.selfieCamera;
                    addAndEditItemProvider.loudspeakerController.text =
                        specs.loudspeaker;
                    addAndEditItemProvider.earphoneJackController.text =
                        specs.earphoneJack;
                    addAndEditItemProvider.featuresController.text =
                        specs.features;
                    addAndEditItemProvider.colorsController.text = specs.colors;
                    addAndEditItemProvider.batteryTypeController.text =
                        specs.batteryType;
                    addAndEditItemProvider.chargingController.text =
                        specs.charging;
                    addAndEditItemProvider.freeShippingController.text =
                        oneItemProvider.voucher.freeShipping;
                    addAndEditItemProvider.discountController.text =
                        oneItemProvider.voucher.discount.toString();

                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddAndEditScreen()));
                  },
                  child: Icon(
                    Icons.edit,
                    color: Colors.white,
                  ))
            ],
          ),
          body: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Column(
                      children: [
                        oneItemProvider.imageUrl.isEmpty ?? true
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : Image(
                                fit: BoxFit.contain,
                                image: NetworkImage(oneItemProvider.imageUrl),
                                height:
                                    MediaQuery.of(context).size.height * 0.4,
                              ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          oneItemProvider.name,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          voucher.discount != 0
                              ? 'Now at ₱ ${oneItemProvider.price.toString()} only!'
                              : '₱ ${oneItemProvider.price.toString()}',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.red),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Specifications',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _buildSpecs(addAndEditItemProvider),
                  SizedBox(
                    height: 20,
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Reviews',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _buildReview(addAndEditItemProvider)
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildSpecs(AddAndEditItemProvider addAndEditItemProvider) {
    return Column(
      children: [
        CustomLabel(
          label: 'Dimension',
          input: specification.dimensions,
        ),
        CustomLabel(
          label: 'Weight',
          input: specification.weight,
        ),
        CustomLabel(
          label: 'Build',
          input: specification.build,
        ),
        CustomLabel(
          label: 'Display',
          input: specification.displayType,
        ),
        CustomLabel(
          label: 'Size',
          input: specification.size,
        ),
        CustomLabel(
          label: 'Resolution',
          input: specification.resolution,
        ),
        CustomLabel(
          label: 'Protection',
          input: specification.protection,
        ),
        CustomLabel(
          label: 'Operating System',
          input: specification.operatingSystem,
        ),
        CustomLabel(
          label: 'Chipset',
          input: specification.chipset,
        ),
        CustomLabel(
          label: 'CPU',
          input: specification.cpu,
        ),
        CustomLabel(
          label: 'GPU',
          input: specification.gpu,
        ),
        CustomLabel(
          label: 'Internal',
          input: specification.internal,
        ),
        CustomLabel(
          label: 'Main Camera',
          input: specification.mainCamera,
        ),
        CustomLabel(
          label: 'Selfie Camera',
          input: specification.selfieCamera,
        ),
        CustomLabel(
          label: 'Loudspeaker',
          input: specification.loudspeaker,
        ),
        CustomLabel(
          label: 'Earphone Jack',
          input: specification.earphoneJack,
        ),
        CustomLabel(
          label: 'Features',
          input: specification.features,
        ),
        CustomLabel(
          label: 'Colors',
          input: specification.colors,
        ),
        CustomLabel(
          label: 'Battery Type',
          input: specification.batteryType,
        ),
        CustomLabel(
          label: 'Charging',
          input: specification.charging,
        ),
      ],
    );
  }

  Widget _buildReview(AddAndEditItemProvider addAndEditItemProvider) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(20),
          height: 50 * reviews.length.toDouble() + 40,
          decoration: BoxDecoration(
            color: Color(0xFFe1effa),
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              itemCount: reviews.length,
              itemBuilder: (context, int index) {
                return SizedBox(
                  height: 50,
                  child: Row(
                    children: [
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.blue),
                        child: Center(
                            child: Text(
                          'A',
                          style: TextStyle(color: Colors.white),
                        )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Container(
                            height: 40,
                            width: MediaQuery.of(context).size.width * 0.6,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                  child: Text(
                                    reviews[index].comment,
                                    maxLines: 3,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ],
                            )),
                      )
                    ],
                  ),
                );
              }),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          padding: EdgeInsets.fromLTRB(20, 10, 10, 5),
          height: 40,
          decoration: BoxDecoration(
              border: Border.all(width: 0.1),
              borderRadius: BorderRadius.circular(10),
              color: Colors.white),
          child: TextFormField(
            style: TextStyle(fontSize: 12),
            decoration: InputDecoration(
              hintText: 'Write a review ...',
              hintStyle: TextStyle(fontSize: 12),
              border: InputBorder.none,
            ),
          ),
        )
      ],
    );
  }
}
