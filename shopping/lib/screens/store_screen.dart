import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/item_model.dart';
import 'package:shopping/provider/add_and_edit_item_provider.dart';
import 'package:shopping/provider/checkbox_provider.dart';
import 'package:shopping/provider/one_store_provider.dart';
import 'package:shopping/widgets/item_card_widget.dart';

import 'add_and_edit_item_screen.dart';

class OneStoreScreen extends StatefulWidget {
  @override
  _OneStoreScreenState createState() => _OneStoreScreenState();
}

class _OneStoreScreenState extends State<OneStoreScreen> {
  List<Item> items = [];
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      final storeProvider = watch(oneStoreCNP);
      final addAndEditItemProvider = watch(addAndEditItemCNP);
      final checkboxProvider = watch(checkboxCNP);
      items = storeProvider.items;
      return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          centerTitle: false,
          title: Text(
            'Store',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          child: Container(
            height: 750,
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                _buildTopWidget(storeProvider),
                SizedBox(
                  height: 20,
                ),
                _buildBottomWidget(
                    storeProvider, addAndEditItemProvider, checkboxProvider)
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget _buildTopWidget(storeProvider) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 120,
      decoration: BoxDecoration(
        color: Color(0xFFe1effa),
        borderRadius: BorderRadius.circular(10),
      ),
      child: storeProvider.imageUrl.isEmpty ?? true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Row(
              children: [
                Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 3),
                              blurRadius: 2,
                              color: Color(0xFF697b96))
                        ]),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: NetworkImage(storeProvider.imageUrl),
                              fit: BoxFit.fitHeight)),
                    )),
                SizedBox(width: 20),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      storeProvider.name,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        ElevatedButton(
                            onPressed: () {},
                            child: Text(
                              '+ Follow',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            )),
                        SizedBox(
                          width: 10,
                        ),
                        ElevatedButton(
                            onPressed: () {},
                            child: Row(
                              children: [
                                Icon(
                                  Icons.chat_bubble,
                                  size: 12,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  'Chat',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12),
                                ),
                              ],
                            )),
                      ],
                    )
                  ],
                )
              ],
            ),
    );
  }

  Widget _buildBottomWidget(
      GetOneStoreProvider storeProvider,
      AddAndEditItemProvider addAndEditItemProvider,
      CheckboxProvider checkboxProvider) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'Items',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 20,
              ),
              InkWell(
                onTap: () async {
                  print('tapped add button');
                  await addAndEditItemProvider.getTotalItemsNumber();
                  addAndEditItemProvider.setStoreName(storeProvider.name);
                  addAndEditItemProvider
                      .setStoreId(storeProvider.id.toString());
                  addAndEditItemProvider.setIds(
                      (addAndEditItemProvider.numberOfItems + 1).toString());

                  // clearing the fields if adding item
                  addAndEditItemProvider.clearAllForms();
                  checkboxProvider.choosenList.clear();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => AddAndEditScreen(
                                formType: 'ADD',
                              )));
                },
                child: Container(
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.blue),
                        shape: BoxShape.circle,
                        color: Colors.transparent),
                    child: Icon(
                      Icons.add,
                      color: Colors.blue,
                      size: 15,
                    )),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: items.isEmpty
                ? Center(
                    child: Text(
                      'No Items Available',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue),
                    ),
                  )
                : GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2),
                    children: items.map((item) {
                      return ItemCard(item: item);
                    }).toList(),
                  ),
          )
        ],
      ),
    );
  }
}
