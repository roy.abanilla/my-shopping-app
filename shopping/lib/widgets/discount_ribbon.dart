import 'package:flutter/material.dart';

class DiscountRibbonWidget extends StatelessWidget {
  final int discount;
  final double diameter;
  final double fontsize;

  const DiscountRibbonWidget(
      {Key key,
      @required this.discount,
      @required this.diameter,
      @required this.fontsize})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: diameter,
      width: diameter,
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
      child: Center(
        child: Text(
          '${discount.toString()}%\nOFF',
          style: TextStyle(color: Colors.white, fontSize: fontsize),
        ),
      ),
    );
  }
}
