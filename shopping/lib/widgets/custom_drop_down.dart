import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomDropDown extends StatefulWidget {
  String variable;
  final List<String> list;
  Function function;

  CustomDropDown({this.variable, this.list, this.function});
  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.6,
      height: 30,
      padding: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5)),
      child: DropdownButton(
          underline: SizedBox(),
          isExpanded: true,
          iconSize: 30,
          value: widget.variable,
          onChanged: widget.function,
          items: widget.list.map((value) {
            return DropdownMenuItem(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(fontSize: 12),
                ));
          }).toList()),
    );
  }
}
