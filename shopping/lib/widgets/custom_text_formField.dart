import 'package:flutter/material.dart';

class CustomTextFormField extends StatefulWidget {
  final TextEditingController controller;
  final String label;
  final String hintText;
  final TextInputType textInputType;
  final Function validator;

  const CustomTextFormField(
      {Key key,
      @required this.controller,
      @required this.label,
      @required this.hintText,
      this.textInputType = TextInputType.text,
      this.validator})
      : super(key: key);
  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _isEmpty = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              widget.label,
            ),
            Spacer(),
            SizedBox(
              height: _isEmpty ? 50 : 30,
              width: MediaQuery.of(context).size.width * .6,
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    setState(() {
                      _isEmpty = true;
                    });
                    return '*Required Fields';
                  }
                  return null;
                },
                keyboardType: widget.textInputType,
                controller: widget.controller,
                style: TextStyle(fontSize: 12),
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                    errorStyle:
                        TextStyle(fontSize: 10, fontStyle: FontStyle.italic),
                    contentPadding: EdgeInsets.all(5),
                    alignLabelWithHint: true,
                    hintText: widget.hintText,
                    hintStyle:
                        TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    border: OutlineInputBorder()),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}
