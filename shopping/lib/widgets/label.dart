import 'package:flutter/material.dart';

class CustomLabel extends StatelessWidget {
  final String label;
  final String input;

  const CustomLabel({
    Key key,
    @required this.label,
    @required this.input,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.30,
                  child: Text(
                    label,
                    style: TextStyle(
                        color: Color(0xFF9B9B9B),
                        fontFamily: 'Barlow-Regular',
                        fontSize: 13.0),
                    maxLines: 2,
                  )),
              SizedBox(width: 10.0),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.55,
                child: Text(
                  input.isEmpty ? 'N/A' : input,
                  style: TextStyle(
                      color: Color(0xFF222A34),
                      fontFamily: 'Barlow-Medium',
                      fontSize: 13.0),
                  maxLines: 4,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          )
        ],
      );
}
