import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/item_model.dart';
import 'package:shopping/provider/one_item_provider.dart';
import 'package:shopping/screens/item_screen.dart';
import 'package:shopping/widgets/discount_ribbon.dart';

class ItemCard extends StatefulWidget {
  final Item item;

  const ItemCard({Key key, this.item}) : super(key: key);
  @override
  _ItemCardState createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final oneItemProvider = watch(oneItemCNP);
        return InkWell(
          onTap: () async {
            print('tapped ${widget.item.name}');
            print('item id: ${widget.item.id}');
            oneItemProvider.setItemId(widget.item.id);
            await oneItemProvider.getItem();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => ItemScreen()));
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: 150,
            width: 100,
            decoration: BoxDecoration(
              color: Color(0xFFe1effa),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: widget.item.discount != 0
                      ? MainAxisAlignment.end
                      : MainAxisAlignment.center,
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image(
                            fit: BoxFit.fitHeight,
                            width: 70,
                            height: 80,
                            image: NetworkImage(widget.item.imageUrl))),
                    if (widget.item.discount != 0)
                      SizedBox(
                        width: 10,
                      ),
                    if (widget.item.discount != 0)
                      DiscountRibbonWidget(
                        discount: widget.item.discount,
                        diameter: 35,
                        fontsize: 8,
                      )
                  ],
                ),
                Text(
                  widget.item.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: widget.item.name.length > 15 ? 12 : 14,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  '₱ ${widget.item.price.toString()}',
                  style: TextStyle(color: Colors.red),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
