import 'package:flutter/material.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.10,
        ),
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                  maxRadius: 50.0,
                  backgroundImage:
                      NetworkImage('https://picsum.photos/250?image=1027')),
              SizedBox(height: 15.0),
              Text(
                'Sample Name',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFFfcd303),
                ),
              ),
              Text(
                'sample@gmail.com',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.normal,
                  color: Color(0xFFfcd303),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20.0,
        ),
        _buildListTile(
            'Profile',
            Icon(
              Icons.person,
              color: Color(0xFFfcd303),
            )),
        _buildListTile(
            'Rate Us?',
            Icon(
              Icons.star,
              color: Color(0xFFfcd303),
            )),
        _buildListTile(
            'Settings',
            Icon(
              Icons.settings,
              color: Color(0xFFfcd303),
            )),
      ],
    );
  }

  Widget _buildListTile(String text, Icon icon) {
    return ListTile(
        onTap: () {
          print('$text Selected');
        },
        title: Text(
          text,
          style: TextStyle(color: Color(0xFFfcd303)),
        ),
        leading: icon);
  }
}
