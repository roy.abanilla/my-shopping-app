class Voucher {
  String id;
  String freeShipping;
  int discount;

  Voucher({this.id, this.freeShipping, this.discount});

  factory Voucher.fromJson(dynamic json) {
    return Voucher(
        id: json['id'] as String,
        freeShipping: json['free_shipping'] as String,
        discount: json['discount'] as int);
  }
}
