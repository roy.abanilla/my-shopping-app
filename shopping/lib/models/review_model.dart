class Review {
  String id;
  String cellphoneId;
  String comment;

  Review({this.id, this.cellphoneId, this.comment});
  factory Review.fromJson(dynamic json) {
    return Review(
        id: json['id'] as String,
        cellphoneId: json['cellphone_id'] as String,
        comment: json['comment'] as String);
  }
}
