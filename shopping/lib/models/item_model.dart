class Item {
  String id;
  String storeID;
  String name;
  String imageUrl;
  int price;
  int discount;
  String freeShipping;

  Item(
      {this.id,
      this.storeID,
      this.name,
      this.imageUrl,
      this.price,
      this.discount,
      this.freeShipping});

  factory Item.fromJson(dynamic json) {
    return Item(
        id: json['id'] as String,
        storeID: json['store_id'] as String,
        name: json['name'] as String,
        imageUrl: json['imageUrl'] as String,
        price: json['price'] as int,
        discount: json['Voucher']['discount'] as int,
        freeShipping: json['Voucher']['free_shipping'] as String);
  }
}
