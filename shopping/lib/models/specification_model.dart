class Specification {
  String id;
  String dimensions;
  String weight;
  String build;
  String displayType;
  String size;
  String resolution;
  String protection;
  String operatingSystem;
  String chipset;
  String cpu;
  String gpu;
  String internal;
  String mainCamera;
  String selfieCamera;
  String loudspeaker;
  String earphoneJack;
  String features;
  String colors;
  String batteryType;
  String charging;

  Specification(
      {this.id,
      this.dimensions,
      this.weight,
      this.build,
      this.displayType,
      this.size,
      this.resolution,
      this.protection,
      this.operatingSystem,
      this.chipset,
      this.cpu,
      this.gpu,
      this.internal,
      this.mainCamera,
      this.selfieCamera,
      this.loudspeaker,
      this.earphoneJack,
      this.features,
      this.colors,
      this.batteryType,
      this.charging});

  factory Specification.fromJson(dynamic json) {
    return Specification(
      id: json['id'] as String,
      dimensions: json['dimensions'] as String,
      weight: json['weight'] as String,
      build: json['build'] as String,
      displayType: json['display_type'] as String,
      size: json['size'] as String,
      resolution: json['resolution'] as String,
      protection: json['protection'] as String,
      operatingSystem: json['OS'] as String,
      chipset: json['chipset'] as String,
      cpu: json['CPU'] as String,
      gpu: json['GPU'] as String,
      internal: json['internal'] as String,
      mainCamera: json['main_camera'] as String,
      selfieCamera: json['selfie_camera'] as String,
      loudspeaker: json['loudspeaker'] as String,
      earphoneJack: json['earphone_jack'] as String,
      features: json['features'] as String,
      colors: json['colors'] as String,
      batteryType: json['battery_type'] as String,
      charging: json['charging'] as String,
    );
  }
}
