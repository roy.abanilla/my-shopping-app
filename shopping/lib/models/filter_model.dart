class ChooseFilter {
  String title;
  bool value;

  ChooseFilter({this.title, this.value = false});
}
