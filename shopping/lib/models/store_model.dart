class OneStore {
  String id;
  String name;
  String imageUrl;

  OneStore({this.id, this.name, this.imageUrl});

  factory OneStore.fromJson(dynamic json) {
    return OneStore(
      id: json['id'] as String,
      name: json['name'] as String,
      imageUrl: json['imageUrl'] as String,
    );
  }
}
