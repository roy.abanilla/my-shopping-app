import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final userCNP = ChangeNotifierProvider<UserProvider>((ref) => UserProvider());

// Get One Item
class UserProvider extends ChangeNotifier {
  String _id;
  String _email;
  String _imageUrl;

  String get id => _id;
  String get email => _email;
  String get imageUrl => _imageUrl;

  void setId(String value) {
    _id = value;
    notifyListeners();
  }

  void setEmail(String value) {
    _email = value;
    notifyListeners();
  }

  void setImageUrl(String value) {
    _imageUrl = value;
    notifyListeners();
  }

  void clearAll() {
    _id = null;
    _email = null;
    _imageUrl = null;
    notifyListeners();
  }

  notifyListeners();
}
