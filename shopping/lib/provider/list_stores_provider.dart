import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shopping/graphql/queries.dart';
import 'package:shopping/helper/graphql.dart';
import 'package:shopping/models/store_model.dart';

final getListOfStore = ChangeNotifierProvider<GetListOfStoreProvider>(
    (ref) => GetListOfStoreProvider());

class GetListOfStoreProvider extends ChangeNotifier {
  List<OneStore> _listOfStore = [];

  List<OneStore> get listOfStore => _listOfStore;

  final GraphQLClient _graphqlClient = client.value;

  Future getListOfStore() async {
    final QueryResult result = await _graphqlClient.query(QueryOptions(
        document: gql(getAllStore),
        variables: null,
        fetchPolicy: FetchPolicy.noCache));

    if (result.hasException) {
      return Text('No data');
    } else {
      final list = result.data['allStores'] as List<dynamic>;
      _listOfStore = list.map((store) => OneStore.fromJson(store)).toList();
    }
  }

  notifyListeners();
}
