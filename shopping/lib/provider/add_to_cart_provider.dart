import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final addToCartCNP =
    ChangeNotifierProvider<AddToCartProvider>((ref) => AddToCartProvider());

// Get List of All add to carts Items
class AddToCartProvider extends ChangeNotifier {
  List<String> _addToCartList = [];

  List<String> get addToCartList => _addToCartList;

  void addToCart(String value) {
    _addToCartList.add(value);
    notifyListeners();
  }

  void removeToCart(String value) {
    _addToCartList.remove(value);
    notifyListeners();
  }

  int cartCount(String value) {
    return _addToCartList.where((element) => element == value).length;
  }

  notifyListeners();
}
