import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shopping/graphql/mutations.dart';
import 'package:shopping/graphql/queries.dart';
import 'package:shopping/helper/graphql.dart';

final addAndEditItemCNP = ChangeNotifierProvider<AddAndEditItemProvider>(
    (ref) => AddAndEditItemProvider());

// Get One Item
class AddAndEditItemProvider extends ChangeNotifier {
  String _id;
  String _storeId;
  String _specificationId;
  String _voucherId;
  String _storeName;
  bool _isFormValid;

  // getting number of Items
  int _numberOfItems;
  int get numberOfItems => _numberOfItems;
  String get storeId => _storeId;
  String get itemId => _id;
  String get specsId => _specificationId;
  String get voucherId => _voucherId;
  bool get isFormValid => _isFormValid;

  // main specs
  final TextEditingController _itemNameController = TextEditingController();
  final TextEditingController _imageUrlController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _stocksController = TextEditingController();

  // specs
  final TextEditingController _dimensionsController = TextEditingController();
  final TextEditingController _weightController = TextEditingController();
  final TextEditingController _buildController = TextEditingController();
  final TextEditingController _displayTypeController = TextEditingController();
  final TextEditingController _sizeController = TextEditingController();
  final TextEditingController _resolutionController = TextEditingController();
  final TextEditingController _protectionController = TextEditingController();
  final TextEditingController _osController = TextEditingController();
  final TextEditingController _chipsetController = TextEditingController();
  final TextEditingController _cpuController = TextEditingController();
  final TextEditingController _gpuController = TextEditingController();
  final TextEditingController _internalController = TextEditingController();
  final TextEditingController _mainCameraController = TextEditingController();
  final TextEditingController _selfieCameraController = TextEditingController();
  final TextEditingController _loudspeakerController = TextEditingController();
  final TextEditingController _earphoneJackController = TextEditingController();
  final TextEditingController _featuresController = TextEditingController();
  final TextEditingController _colorsController = TextEditingController();
  final TextEditingController _batteryTypeController = TextEditingController();
  final TextEditingController _chargingController = TextEditingController();

  // voucher
  final TextEditingController _freeShippingController = TextEditingController();
  final TextEditingController _discountController = TextEditingController();

  // main getters
  String get storeName => _storeName;
  TextEditingController get itemNameController => _itemNameController;
  TextEditingController get imageUrlController => _imageUrlController;
  TextEditingController get priceController => _priceController;
  TextEditingController get stocksController => _stocksController;

  // specs getter
  TextEditingController get dimensionsController => _dimensionsController;
  TextEditingController get weightController => _weightController;
  TextEditingController get buildController => _buildController;
  TextEditingController get displayTypeController => _displayTypeController;
  TextEditingController get sizeController => _sizeController;
  TextEditingController get resolutionController => _resolutionController;
  TextEditingController get protectionController => _protectionController;
  TextEditingController get osController => _osController;
  TextEditingController get chipsetController => _chipsetController;
  TextEditingController get cpuController => _cpuController;
  TextEditingController get gpuController => _gpuController;
  TextEditingController get internalController => _internalController;
  TextEditingController get mainCameraController => _mainCameraController;
  TextEditingController get selfieCameraController => _selfieCameraController;
  TextEditingController get loudspeakerController => _loudspeakerController;
  TextEditingController get earphoneJackController => _earphoneJackController;
  TextEditingController get featuresController => _featuresController;
  TextEditingController get colorsController => _colorsController;
  TextEditingController get batteryTypeController => _batteryTypeController;
  TextEditingController get chargingController => _chargingController;

  // voucher getter
  TextEditingController get freeShippingController => _freeShippingController;
  TextEditingController get discountController => _discountController;

  void setStoreName(String value) {
    _storeName = value;
    print('store name : ${value.toString()}');
    notifyListeners();
  }

  void setStoreId(String value) {
    _storeId = value;
    print('store ID : ${_storeId.toString()}');
    notifyListeners();
  }

  void setIds(String value) {
    _id = value;
    _specificationId = value;
    _voucherId = value;
    print('all Ids : ${value.toString()}');
    notifyListeners();
  }

  void clearAllForms() {
    _itemNameController.clear();
    _imageUrlController.clear();
    _priceController.clear();
    _stocksController.clear();

    _itemNameController.clear();
    _imageUrlController.clear();
    _priceController.clear();
    _stocksController.clear();
    _dimensionsController.clear();
    _weightController.clear();
    _buildController.clear();
    _displayTypeController.clear();
    _sizeController.clear();
    _resolutionController.clear();
    _protectionController.clear();
    _osController.clear();
    _chipsetController.clear();
    _cpuController.clear();
    _gpuController.clear();
    _internalController.clear();
    _mainCameraController.clear();
    _selfieCameraController.clear();
    _loudspeakerController.clear();
    _earphoneJackController.clear();
    _featuresController.clear();
    _colorsController.clear();
    _batteryTypeController.clear();
    _chargingController.clear();
    _freeShippingController.clear();
    _discountController.clear();
    notifyListeners();
  }

  final GraphQLClient _graphqlClient = client.value;

  Future getTotalItemsNumber() async {
    final QueryResult result = await _graphqlClient.query(QueryOptions(
        document: gql(getNumberOfItems),
        variables: {},
        fetchPolicy: FetchPolicy.noCache));

    if (result.hasException) {
      print('error in getting number of Items');
      print(result.exception.toString());
      return Text('No data');
    } else {
      print('successful getting number of Items');
      final list = result.data['allItems'] as List<dynamic>;
      _numberOfItems = list.length;
    }
  }

  Future createOneItem() async {
    final QueryResult result = await _graphqlClient.mutate(MutationOptions(
      document: gql(createItem),
      variables: {
        "id": _id,
        "store_id": _storeId,
        "name": _itemNameController.text,
        "imageUrl": _imageUrlController.text,
        "price": int.parse(_priceController.text.toString()),
        "stocks": int.parse(_stocksController.text.toString()),
        "specification_id": _specificationId,
        "voucher_id": _voucherId,
        "dimensions": _dimensionsController.text,
        "weight": _weightController.text,
        "build": _buildController.text,
        "display_type": _displayTypeController.text,
        "size": _sizeController.text,
        "resolution": _resolutionController.text,
        "protection": _protectionController.text,
        "os": _osController.text,
        "chipset": _chipsetController.text,
        "cpu": _cpuController.text,
        "gpu": _gpuController.text,
        "internal": _internalController.text,
        "main_camera": _mainCameraController.text,
        "selfie_camera": _selfieCameraController.text,
        "loudspeaker": _loudspeakerController.text,
        "earphone_jack": _earphoneJackController.text,
        "features": _featuresController.text,
        "colors": _colorsController.text,
        "battery_type": _batteryTypeController.text,
        "charging": _chargingController.text,
        "discount": int.parse(_discountController.text.toString()) ?? 0,
        "free_shipping": _freeShippingController.text
      },
    ));

    if (result.hasException) {
      print('error in creating Item');
      print(result.exception.toString());
      return Text('No data');
    } else {
      print('successful creating Item');
    }
  }

  Future updateOneItem() async {
    final QueryResult result = await _graphqlClient
        .mutate(MutationOptions(document: gql(updateItem), variables: {
      "id": _id,
      "store_id": _storeId,
      "name": _itemNameController.text,
      "imageUrl": _imageUrlController.text,
      "price": int.parse(_priceController.text.toString()),
      "stocks": int.parse(_stocksController.text.toString()),
      "specification_id": _specificationId,
      "voucher_id": _voucherId,
      "dimensions": _dimensionsController.text,
      "weight": _weightController.text,
      "build": _buildController.text,
      "display_type": _displayTypeController.text,
      "size": _sizeController.text,
      "resolution": _resolutionController.text,
      "protection": _protectionController.text,
      "os": _osController.text,
      "chipset": _chipsetController.text,
      "cpu": _cpuController.text,
      "gpu": _gpuController.text,
      "internal": _internalController.text,
      "main_camera": _mainCameraController.text,
      "selfie_camera": _selfieCameraController.text,
      "loudspeaker": _loudspeakerController.text,
      "earphone_jack": _earphoneJackController.text,
      "features": _featuresController.text,
      "colors": _colorsController.text,
      "battery_type": _batteryTypeController.text,
      "charging": _chargingController.text,
      "discount": int.parse(_discountController.text.toString()) ?? 0,
      "free_shipping": _freeShippingController.text ?? 'No'
    }));

    if (result.hasException) {
      print('error in updating Item');
      print(result.exception.toString());
      return Text('No data');
    } else {
      print('successful updating Item');
    }
  }

  notifyListeners();
}
