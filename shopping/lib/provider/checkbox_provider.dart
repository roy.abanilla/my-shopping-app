import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shopping/models/filter_model.dart';

final checkboxCNP =
    ChangeNotifierProvider<CheckboxProvider>((ref) => CheckboxProvider());

class CheckboxProvider extends ChangeNotifier {
  List<String> _choosenList = [];

  List<ChooseFilter> _internalList = [
    ChooseFilter(title: '32GB 3GB RAM'),
    ChooseFilter(title: '64GB 3GB RAM'),
    ChooseFilter(title: '64GB 4GB RAM'),
    ChooseFilter(title: '64GB 6GB RAM'),
    ChooseFilter(title: '128GB 4GB RAM'),
    ChooseFilter(title: '128GB 6GB RAM'),
    ChooseFilter(title: '128GB 8GB RAM'),
    ChooseFilter(title: '256GB 6GB RAM'),
    ChooseFilter(title: '256GB 8GB RAM'),
    ChooseFilter(title: '256GB 12GB RAM'),
  ];

  List<ChooseFilter> get list => _internalList;
  List<String> get choosenList => _choosenList;

  void setChoosenList(List<String> list) {
    _choosenList = list;
    notifyListeners();
  }

  void addToList(String value) {
    _choosenList.add(value);
    notifyListeners();
  }

  void removeToList(String value) {
    _choosenList.remove(value);
    notifyListeners();
  }

  notifyListeners();
}
