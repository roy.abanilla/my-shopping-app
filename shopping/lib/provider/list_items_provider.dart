import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shopping/graphql/queries.dart';
import 'package:shopping/helper/graphql.dart';
import 'package:shopping/models/item_model.dart';

final getListOfItemProvider = ChangeNotifierProvider<GetListOfItemProvider>(
    (ref) => GetListOfItemProvider());

// Get List of All Items
class GetListOfItemProvider extends ChangeNotifier {
  List<Item> _listOfItems = [];
  List<Item> _discountedItems = [];

  List<Item> get listOfItems => _listOfItems;
  List<Item> get discountedItems => _discountedItems;

  final GraphQLClient _graphqlClient = client.value;

  Future getListOfItem() async {
    final QueryResult result = await _graphqlClient.query(QueryOptions(
        document: gql(getAllItems),
        variables: null,
        fetchPolicy: FetchPolicy.noCache));

    if (result.hasException) {
      print(result.exception.toString());
      return Text('No data');
    } else {
      print('successful getting list of items');

      final list = result.data['allItems'] as List<dynamic>;
      _listOfItems = list.map((item) => Item.fromJson(item)).toList();
      _discountedItems =
          _listOfItems.where((item) => item.discount != 0).toList();
      _discountedItems.sort((a, b) => b.discount.compareTo(a.discount));
    }
  }

  notifyListeners();
}
