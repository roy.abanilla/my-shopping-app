import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shopping/graphql/queries.dart';
import 'package:shopping/helper/graphql.dart';
import 'package:shopping/models/item_model.dart';

final oneStoreCNP =
    ChangeNotifierProvider<GetOneStoreProvider>((ref) => GetOneStoreProvider());

// Get One Item
class GetOneStoreProvider extends ChangeNotifier {
  String _id;
  String _name;
  String _imageUrl;
  List<Item> _items;

  String get id => _id;
  String get name => _name;
  String get imageUrl => _imageUrl;
  List<Item> get items => _items;

  void setStoreId(String value) {
    _id = value;
    notifyListeners();
  }

  void clearAll() {
    _items = [];
    notifyListeners();
  }

  final GraphQLClient _graphqlClient = client.value;

  Future getStore() async {
    final QueryResult result = await _graphqlClient.query(QueryOptions(
        document: gql(getOneStore),
        variables: {'id': _id},
        fetchPolicy: FetchPolicy.noCache));

    if (result.hasException) {
      print('error in getting One Store');
      print(result.exception.toString());
      return Text('No data');
    } else {
      print('success getting one Store');
      final data = result.data['Store'];
      final items = result.data['Store']['Items'] as List<dynamic>;
      _name = data['name'] as String;
      _imageUrl = data['imageUrl'] as String;
      _items = items.map((item) => Item.fromJson(item)).toList();
    }
  }

  notifyListeners();
}
