import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shopping/graphql/queries.dart';
import 'package:shopping/helper/graphql.dart';
import 'package:shopping/models/review_model.dart';
import 'package:shopping/models/specification_model.dart';
import 'package:shopping/models/voucher_model.dart';

final oneItemCNP =
    ChangeNotifierProvider<GetOneItemProvider>((ref) => GetOneItemProvider());

// Get One Item
class GetOneItemProvider extends ChangeNotifier {
  String _id;
  String _name;
  String _storeId;
  String _storeName;
  String _imageUrl;
  int _price;
  int _stocks;
  Specification _specification;
  Voucher _voucher;
  List<Review> _reviews;

  String get id => _id;
  String get name => _name;
  String get storeName => _storeName;
  String get storeId => _storeId;
  String get imageUrl => _imageUrl;
  int get price => _price;
  int get stocks => _stocks;
  Specification get specification => _specification;
  Voucher get voucher => _voucher;
  List<Review> get reviews => _reviews;

  void setItemId(String value) {
    _id = value;
    notifyListeners();
  }

  final GraphQLClient _graphqlClient = client.value;

  Future getItem() async {
    final QueryResult result = await _graphqlClient.query(QueryOptions(
        document: gql(getOneItem),
        variables: {'id': _id},
        fetchPolicy: FetchPolicy.noCache));

    if (result.hasException) {
      print('error in getting One Item');
      print(result.exception.toString());
      return Text('No data');
    } else {
      final data = result.data['Item'];
      _name = data['name'] as String;
      _imageUrl = data['imageUrl'] as String;
      _price = data['price'] as int;
      _stocks = data['stocks'] as int;

      final specs = data['Specification'] as dynamic;
      _specification = Specification.fromJson(specs);

      final voucher = data['Voucher'] as dynamic;
      _voucher = Voucher.fromJson(voucher);

      final list = data['Reviews'] as List<dynamic>;
      _reviews = list.map((review) => Review.fromJson(review)).toList();

      final storeList = result.data['allStores'] as List<dynamic>;

      for (var i = 0; i < storeList.length; i++) {
        List<dynamic> itemList = storeList[i]['Items'] as List<dynamic>;
        for (var j = 0; j < itemList.length; j++) {
          if (itemList[j]['id'].toString() == _id) {
            _storeName = storeList[i]['name'] as String;
            _storeId = storeList[i]['id'] as String;
          }
        }
      }
    }
  }

  notifyListeners();
}
