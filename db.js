
 module.exports = {
    store: [ 
        { 
            id: 1,
            name: "Realme",
            imageUrl: "https://res-1.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco/rv1vcuxexaynvqkefdjv"
        },
        { 
            id: 2,
            name: "Samsung",
            imageUrl: "https://s3.amazonaws.com/cdn.designcrowd.com/blog/2017/April/35-Famous-Circle-Logos/13_300.png"
        },
        { 
            id: 3,
            name: "Apple",
            imageUrl: "https://www.pngkey.com/png/full/901-9019597_apple-logo-riot-chat-logo.png"
        },
        { 
            id: 4,
            name: "Huawei",
            imageUrl: "https://3axis.co/user-images/e1gkwldo.png"
        },
        { 
            id: 5,
            name: "Xiaomi",
            imageUrl: "https://i.pinimg.com/originals/b5/2c/6d/b52c6d4e3fc09609c280957e4b9ec79f.png"
        },
        { 
            id: 6,
            name: "LG",
            imageUrl: "https://i.pinimg.com/originals/ba/bc/88/babc8834726480671d13ce482015d279.jpg"
        },
        { 
            id: 7,
            name: "Cherry Mobile",
            imageUrl: "https://pbs.twimg.com/profile_images/1086180781058211841/mG82Pilw.jpg"
        },
        { 
            id: 8,
            name: "Infinix",
            imageUrl: "https://assets.mspimages.in/wp-content/uploads/2020/07/Infinix-696x464.jpg"
        },
        { 
            id: 9,
            name: "Vivo",
            imageUrl: "https://i.pinimg.com/originals/b3/01/56/b3015656a7fe6ff38da6841841660e34.png"
        },
        { 
            id: 10,
            name: "Oppo",
            imageUrl: "https://www.wallpapertip.com/wmimgs/63-638730_oppo-logo-circle.jpg"
        },
    ],
    items: [
        {   
            id: 1, 
            store_id: 1,
            name: "Realme 3", 
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-3.jpg",
            price: 6999,
            stocks: 124,
            specification_id: 1,
            voucher_id: 1
        },
        {   
            id: 2, 
            store_id: 1,
            name: "Realme C15",
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-c15-r1.jpg", 
            price: 6490,
            stocks: 181,
            specification_id: 2,
            voucher_id: 2
        },
        {   
            id: 3, 
            store_id: 1,
            name: "Realme 7i", 
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-7i.jpg",
            price: 9990,
            stocks: 109,
            specification_id: 3,
            voucher_id: 3
        },
        {   
            id: 4, 
            store_id: 1,
            name: "Realme 5", 
            imageUrl: "https://www.pinoytechnoguide.com/wp-content/uploads/2019/08/Realme-5-Pro.jpg",
            price: 6990,
            stocks: 109,
            specification_id: 4,
            voucher_id: 4
        },
        {   
            id: 5, 
            store_id: 1,
            name: "Realme 6 Pro",
            imageUrl: "https://d2pa5gi5n2e1an.cloudfront.net/global/images/product/mobilephones/Realme_6_Pro/Realme_6_Pro_L_1.jpg", 
            price: 13990,
            stocks: 82,
            specification_id: 5,
            voucher_id: 5
        },
        {   
            id: 6, 
            store_id: 1,
            name: "Realme 7 Pro", 
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-7-pro.jpg",
            price: 15990,
            stocks: 210,
            specification_id: 6,
            voucher_id: 6
        },
        {   
            id: 7, 
            store_id: 1,
            name: "Realme C17", 
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-c17.jpg",
            price: 6000,
            stocks: 29,
            specification_id: 7,
            voucher_id: 7
        },
        {   
            id: 8, 
            store_id: 1,
            name: "Realme X50 Pro 5G",
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-x50-pro-5g-.jpg",
            price: 39948,
            stocks: 123,
            specification_id: 8,
            voucher_id: 8
        },
        {   
            id: 9, 
            store_id: 1,
            name: "Realme Narzo 20",
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-narzo-20.jpg",
            price: 7990,
            stocks: 1301,
            specification_id: 9,
            voucher_id: 9
        },
        {   
            id: 10, 
            store_id: 1,
            name: "Realme C11",
            imageUrl: "https://fdn2.gsmarena.com/vv/bigpic/realme-c11.jpg",
            price: 4990,
            stocks: 63,
            specification_id: 10,
            voucher_id: 10
        },
    ],
    specifications: [
        {   
            id: 1,
            dimensions: "156.1 x 75.6 x 8.3 mm (6.15 x 2.98 x 0.33 in)",
            weight: "175 g (6.17 oz)",
            build: "Glass front (Gorilla Glass 3), plastic back, plastic frame",
            display_type: "IPS LCD",
            size: "6.22 inches, 96.6 cm2 (~81.8% screen-to-body ratio)",
            resolution: "720 x 1520 pixels, 19:9 ratio (~270 ppi density)",
            protection: "Corning Gorilla Glass 3",
            OS: "Android 9 (Pie)",
            chipset: "Mediatek MT6771 Helio P60 (12 nm)",
            CPU: "Octa-core (4x2.0 GHz Cortex-A73 & 4x2.0 GHz Cortex-A53)",
            GPU: "Mali-G72 MP3",
            internal: "32GB 3GB RAM,64GB 3GB RAM,64GB 4GB RAM",
            main_camera: "13 MP, f/1.8, 1/3.1in, 1.12µm, PDAF",
            selfie_camera: "13 MP, f/2.0, (wide), 1/3.1in, 1.12µm",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features: "Fingerprint (rear-mounted), accelerometer, proximity, compass",
            colors: "Black, Dynamic Black, Radiant Blue, Diamond Red",
            battery_type: "Li-Po 4230 mAh, non-removable",
            charging: "Charging 10W"
        },
        {   
            id: 2,
            dimensions: "164.5 x 75.9 x 9.8 mm (6.48 x 2.99 x 0.39 in)",
            weight: "209 g (7.37 oz)",
            build: "Glass front, plastic frame, plastic back",
            display_type: "IPS LCD",
            size: "6.5 inches, 102.0 cm2 (~81.7% screen-to-body ratio)",
            resolution: "720 x 1600 pixels, 20:9 ratio (~270 ppi density)",
            protection: "Corning Gorilla Glass",
            OS: "Android 10",
            chipset: "MediaTek Helio G35 (12 nm)",
            CPU: "Octa-core (4x2.3 GHz Cortex-A53 & 4x1.8 GHz Cortex-A53)",
            GPU: "PowerVR GE8320",
            internal: "32GB 3GB RAM,64GB 3GB RAM,64GB 4GB RAM,128GB 4GB RAM",
            main_camera: "13 MP, f/2.2, (wide), PDAF",
            selfie_camera: "13 MP, f/2.0, (wide), 1/3.1in, 1.12µm",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features: "Fingerprint (rear-mounted), accelerometer, proximity, compass",
            colors:  "Marine Blue, Seagull Silver",
            battery_type: "Li-Po 6000 mAh, non-removable",
            charging: "Fast charging 18W"
        },
        {   
            id: 3,
            dimensions: "164.1 x 75.5 x 8.9 mm (6.46 x 2.97 x 0.35 in)",
            weight: "188 g (6.63 oz)",
            build: "Glass front, plastic frame, plastic back",
            display_type: "IPS LCD, 90Hz",
            size: "6.5 inches, 102.0 cm2 (~82.3% screen-to-body ratio)",
            resolution: "720 x 1600 pixels, 20:9 ratio (~270 ppi density)",
            protection: "Corning Gorilla Glass",
            OS: "Android 10",
            chipset: "Qualcomm SM6115 Snapdragon 662 (11 nm)",
            CPU: "Octa-core (4x2.0 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver)",
            GPU: "Adreno 610",
            internal: "64GB 4GB RAM,128GB 4GB RAM,128GB 8GB RAM",
            main_camera: "64 MP, f/1.8, 26mm (wide), 1/1.73in, 0.8µm, PDAF",
            selfie_camera: "16 MP, f/2.1, 26mm (wide), 1/3in, 1.0µm",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features: "Fingerprint (rear-mounted), accelerometer, gyro, proximity, compass",
            colors:  "Aurora Green, Polar Blue",
            battery_type: "Li-Po 5000 mAh, non-removable",
            charging: "Fast charging 18W",
        },
        {   
            id: 4,  
            dimensions: "164.4 x 75.6 x 9.3 mm (6.47 x 2.98 x 0.37 in)",
            weight: "198 g (6.98 oz)",
            build: "Glass front (Gorilla Glass 3+), plastic back, plastic frame",
            display_type: "IPS LCD",
            size: "6.5 inches, 102.8 cm2 (~82.7% screen-to-body ratio)",
            resolution: "720 x 1600 pixels, 20:9 ratio (~269 ppi density)",
            protection: "Corning Gorilla Glass 3+",
            OS: "Android 9 (Pie)",
            chipset: "Qualcomm SDM665 Snapdragon 665 (11 nm)",
            CPU: "Octa-core (4x2.0 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver)",
            GPU: "Adreno 610",
            internal: "32GB 3GB RAM,64GB 3GB RAM,64GB 4GB RAM,128GB 4GB RAM",
            main_camera: "12 MP, f/1.8, (wide), 1/2.9in, 1.25µm, PDAF",
            selfie_camera: "13 MP, f/2.0, 26mm (wide), 1/3.1in, 1.12µm",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features : "Fingerprint (rear-mounted), accelerometer, gyro, proximity, compass",
            colors: "Crystal Purple, Crystal Blue",
            battery_type: "Li-Po 5000 mAh, non-removable",
            charging: "Charging 10W",
        },
        {   
            id: 5, 
            dimensions: "163.8 x 75.8 x 8.9 mm (6.45 x 2.98 x 0.35 in)",
            weight: "202 g (7.13 oz)",
            build: "Glass front (Gorilla Glass 5), glass back, plastic frame",
            display_type: "IPS LCD",
            size: "6.6 inches, 105.2 cm2 (~84.7% screen-to-body ratio)",
            resolution: "1080 x 2400 pixels, 20:9 ratio (~399 ppi density)",
            protection: "Corning Gorilla Glass 5",
            OS: "Android 10",
            chipset: "Qualcomm SM7125 Snapdragon 720G (8 nm)",
            CPU: "Octa-core (2x2.3 GHz Kryo 465 Gold & 6x1.8 GHz Kryo 465 Silver)",
            GPU: "Adreno 618",
            internal: "64GB 6GB RAM,128GB 6GB RAM,128GB 8GB RAM",
            main_camera: "64 MP, f/1.8, 26mm (wide), 1/1.72in, 0.8µm, PDAF",
            selfie_camera: "16 MP, f/2.1, 26mm (wide), 1/3.06in, 1.0µm",
            loudspeaker: "Yes",
            earphone_jack:	"Yes",
            features: "Fingerprint (side-mounted), accelerometer, gyro, proximity, compass",
            battery_type: "Li-Po 4300 mAh, non-removable",
            charging: "Fast charging 30W, 100% in 57 min (advertised) - VOOC 4.0",
            colors: "Lightning Blue, Lightning Orange, Lightning Red",
            
        },
        {   
            id: 6, 
            dimensions: "160.9 x 74.3 x 8.7 mm (6.33 x 2.93 x 0.34 in)",
            weight: "182 g (6.42 oz)",
            build: "Glass front (Gorilla Glass 3+), plastic frame, plastic back",
            display_type: "Super AMOLED",
            size: "6.4 inches, 98.9 cm2 (~82.7% screen-to-body ratio)",
            resolution: "1080 x 2400 pixels, 20:9 ratio (~411 ppi density)",
            protection: "Corning Gorilla Glass 3+ - Always-on display",
            OS: "Android 10",
            chipset: "Qualcomm SM7125 Snapdragon 720G (8 nm)",
            CPU: "Octa-core (2x2.3 GHz Kryo 465 Gold & 6x1.8 GHz Kryo 465 Silver)",
            GPU: "Adreno 618",
            internal: "128GB 6GB RAM,128GB 8GB RAM",
            main_camera: "64 MP, f/1.8, 26mm (wide), 1/1.73in, 0.8µm, PDAF",
            selfie_camera: "32 MP, f/2.5, 24mm (wide), 1/2.8in, 0.8µm",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features: "Fingerprint (under display, optical), accelerometer, gyro, proximity, compass",
            battery_type: "Li-Po 4500 mAh, non-removable",
            charging: "Fast charging 65W, 50% in 12 min, 100% in 34 min (advertised)",
            colors: "Mirror Blue, Mirror Silver",
        },
        {   
            id: 7,
            dimensions: "164.1 x 75.5 x 8.9 mm (6.46 x 2.97 x 0.35 in)",
            weight: "188 g (6.63 oz)",
            build: "No info",
            display_type: "IPS LCD, 90Hz",
            size: "6.5 inches, 102.0 cm2 (~82.3% screen-to-body ratio)",
            resolution: "720 x 1600 pixels, 20:9 ratio (~270 ppi density)",
            protection: "Corning Gorilla Glass",
            OS: "Android 10",
            chipset: "Qualcomm SM4250 Snapdragon 460 (11 nm)",
            CPU: "Octa-core (4x1.8 GHz Kryo 240 & 4x1.6 GHz Kryo 240)",
            GPU: "Adreno 610",
            internal: "64GB 4GB RAM,128GB 6GB RAM,256GB 6GB RAM",
            main_camera: "13 MP, f/2.2, (wide), 1/3.06in, 1.12µm, PDAF",
            selfie_camera: "8 MP, f/2.0, (wide), 1/4.0in, 1.12µm",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features: "Fingerprint (rear-mounted), accelerometer, proximity, compass",
            battery_type:"Li-Po 5000 mAh, non-removable",
            charging: "Fast charging 18W",
            colors: "Navy Blue, Lake Green",
        },
        {   
            id: 8, 
            dimensions: "159 x 74.2 x 8.9 mm (6.26 x 2.92 x 0.35 in)",
            weight: "205 g (7.23 oz)",
            build: "Glass front (Gorilla Glass 5), glass back (Gorilla Glass 5), aluminum frame",
            display_type: "Super AMOLED, 90Hz, HDR10+",
            size: "6.44 inches, 100.1 cm2 (~84.9% screen-to-body ratio)",
            resolution: "1080 x 2400 pixels, 20:9 ratio (~409 ppi density)",
            protection: "Corning Gorilla Glass 5",
            OS: "Android 10",
            chipset: "Qualcomm SM8250 Snapdragon 865 (7 nm+)",
            CPU: "Octa-core (1x2.84 GHz Kryo 585 & 3x2.42 GHz Kryo 585 & 4x1.8 GHz Kryo 585)",
            GPU: "Adreno 650",
            internal: "128GB 6GB RAM,128GB 8GB RAM,256GB 8GB RAM,256GB 12GB RAM",
            main_camera: "64 MP, f/1.8, 26mm (wide), 1/1.72in, 0.8µm, PDAF",
            selfie_camera: "32 MP, f/2.5, 26mm (wide), 1/2.8in, 0.8µm",
            loudspeaker: "Yes",
            earphone_jack: "No",
            features: "Fingerprint (under display, optical), accelerometer, gyro, proximity, compass",
            battery_type: "Li-Po 4200 mAh, non-removable",
            charging: "Fast charging 65W, 100% in 35 min (advertised) - SuperDart Flash Charge ",
            colors: "Moss Green, Rust Red",
        },
        {   
            id: 9, 
            dimensions: "164.5 x 75.9 x 9.8 mm (6.48 x 2.99 x 0.39 in)",
            weight: "208 g (7.34 oz)",
            build: "No info",
            display_type: "IPS LCD, 450 nits (typ), 560 nits (peak)",
            size: "6.5 inches, 102.0 cm2 (~81.7% screen-to-body ratio)",
            resolution: "720 x 1600 pixels, 20:9 ratio (~270 ppi density)",
            protection: "No info",
            OS: "Android 10",
            chipset: "MediaTek Helio G85 (12nm)",
            CPU: "Octa-core (2x2.0 GHz Cortex-A75 & 6x1.8 GHz Cortex-A55)",
            GPU: "Mali-G52 MC2",
            internal: "64GB 4GB RAM,128GB 4GB RAM",
            main_camera: "48 MP, f/1.8, 26mm (wide), 1/2.0in, 0.8µm, PDAF",
            selfie_camera: "8 MP, f/2.0, 26mm (wide), 1/4in, 1.12µm",
            loudspeaker: "Yes",
            earphone_jack:	"Yes",
            features: "Fingerprint (rear-mounted), accelerometer, proximity, compass",
            battery_type: "Li-Po 6000 mAh, non-removable",
            charging: "Fast charging 18W with Reverse charging",
            colors: "Glory Silver, Victory Blue",
        },
        {   
            id: 10, 
            dimensions: "164.4 x 75.9 x 9.1 mm (6.47 x 2.99 x 0.36 in)",
            weight: "196 g (6.91 oz)",
            build: "No info",
            display_type: "IPS LCD",
            size: "6.5 inches, 103.7 cm2 (~83.1% screen-to-body ratio)",
            resolution: "720 x 1560 pixels, 19.5:9 ratio (~264 ppi density)",
            protection: "No info",
            OS: "Android 10",
            chipset: "MediaTek Helio G35 (12 nm)",
            CPU: "Octa-core (4x2.3 GHz Cortex-A53 & 4x1.8 GHz Cortex-A53)",
            GPU: "PowerVR GE8320",
            internal: "32GB 3GB RAM,64GB 3GB RAM",
            main_camera: "13 MP, f/2.2, (wide), PDAF",
            selfie_camera: "5 MP, f/2.4, 27mm (wide)",
            loudspeaker: "Yes",
            earphone_jack: "Yes",
            features: "Accelerometer, proximity, compass",
            battery_type: "Li-Po 5000 mAh, non-removable",
            charging: "Charging 10W",
            colors: "Mint Green, Pepper Grey",
    },

    ],
    reviews: [
        { 
            id: 999,
            item_id: 1,
            comment: "Realme 3 is the best budget phone. Abot kaya lalo sa mga students na katulad ko"
        },
        { 
            id: 998,
            item_id: 1,
            comment: "REALME ginalinganm mo na naman"
        },
        { 
            id: 997,
            item_id: 2,
            comment: "Comment for Realme C15. Nice"
        },
        { 
            id: 996,
            item_id: 2,
            comment: "C15 is another budget phone"
        },
        { 
            id: 995,
            item_id: 3,
            comment: "Very good!"
        },
        { 
            id: 994,
            item_id: 3,
            comment: "Fantastic!!1"
        },
        { 
            id: 994,
            item_id: 3,
            comment: "Fantastic!!1"
        },
        { 
            id: 993,
            item_id: 4,
            comment: "Abot kaya lalo sa mga students na katulad ko"
        },
        { 
            id: 992,
            item_id: 5,
            comment: "Nice"
        },
        { 
            id: 991,
            item_id: 5,
            comment: "Recommended phone"
        },
        { 
            id: 990,
            item_id: 5,
            comment: "Another budget phone"
        },
        { 
            id: 990,
            item_id: 5,
            comment: "Another budget phone"
        },
        { 
            id: 989,
            item_id: 7,
            comment: "REALME ginalinganm mo na naman"
        },
        { 
            id: 988,
            item_id: 9,
            comment: "Realme lang sakalam"
        },
        { 
            id: 987,
            item_id: 9,
            comment: "Just got this for 20% discount"
        },
        { 
            id: 986,
            item_id: 9,
            comment: "Mabuhay ka totoong ikaw!"
        },
        { 
            id: 985,
            item_id: 9,
            comment: "REALME ginalinganm mo na naman"
        },
        { 
            id: 984,
            item_id: 10,
            comment: "Bilhin niyo na to. Worth it."
        },
    ],
    vouchers: [
       {
        id: 1,
        free_shipping: "Yes",
        discount: 11
       },
       {
        id: 2,
        free_shipping: "Yes",
        discount: 0
       },
       {
        id: 3,
        free_shipping: "Yes",
        discount: 20
       },
       {
        id: 4,
        free_shipping: "Yes",
        discount: 0
       },
       {
        id: 5,
        free_shipping: "No",
        discount: 0
       },
       {
        id: 6,
        free_shipping: "No",
        discount: 10
       },
       {
        id: 7,
        free_shipping: "Yes",
        discount: 8
       },
       {
        id: 8,
        free_shipping: "Yes",
        discount: 0
       },
       {
        id: 9,
        free_shipping: "No",
        discount: 8
       },
       {
        id: 10,
        free_shipping: "Yes",
        discount: 11
       },
    ]
}      
                
            
            
           
           
            
           
          

        

